function [] = print_system_info(sim, ini_dynamics)
%        [] = print_system_info(sim, ini_dynamics)
%

global PN
PN = sim;

Ps = PN.No_of_places;
Ts = PN.No_of_transitions;

if isfield(PN, 'No_of_control_places'), 
    Cs = PN.No_of_control_places;
else Cs = 0;
end;


disp(' ');
disp('======= Process Composition ======= ');
disp(['There are ', int2str(Ps-Cs), ' process places']);
disp(['There are ', int2str(Ts), ' process transitions']);
if Cs, disp(['There are ', int2str(Cs), ' control places']); end;

disp(['Process Petri Net: ', PN.name]);
print_system_info_connection;

X = zeros(1, Ps); % initial assumption
if eq(nargin, 2), 
    X = get_initial_marking(ini_dynamics); % intial marking
    disp(' '); disp('Initial markings on the Process places: ');
    str = markings_string (X, [1 Ps-Cs]); 
    disp(str); disp(' ');
end;

if Cs, 
    print_controller_info(X);
end;

