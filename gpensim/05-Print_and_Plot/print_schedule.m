function [LE] = print_schedule(PN)
% function [] = print_schedule()
% For every resource utilized, this fundtion prints
% the transition that used the resource, start time  and end time
% of utilization

%  Reggie.Davidrajuh@uis.no (c) Version 6.0 (c) 10 july 2012  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

res = PN.system_resources;
Rs = PN.No_of_system_resources;
Ts = PN.No_of_transitions;

LOG = PN.Resource_usage_LOG;
if isempty(LOG), disp('No resources used ...' ); return; end;

[rows] = size(LOG, 1); 
completion_time = LOG(end, end-1);
ST = zeros(1, Rs);  % Station Time

Sum_Resource_Cost = 0;
Sum_Firing_cost = 0;
Sum_Fixed_cost = 0;

% IMPORTANT: LOG file row
% [resource, transition, start_time, end_time, resource_instance]   

for current_res = 1:Rs, % no_of_resources,
    st = 0;   % station time used
    disp('  ');
    resource_name = res(current_res).name; 
    disp([' ***** ', resource_name, ' *****']);
    max_instances = PN.system_resources(current_res).max_instances;
    res_instances_usage = zeros(2, max_instances);
    
    for i = 1:rows,
        if eq(LOG(i,1), current_res),
            % display:  transition name  start_time end_time
            transition_name = PN.global_transitions(LOG(i, 2)).name; 
            start_time = LOG(i,3);  end_time   = LOG(i,4);
            task_time = end_time - start_time;
            res_instance = LOG(i,5);
            res_instances_usage(1, res_instance) = ...
                res_instances_usage(1, res_instance) + 1;
            res_instances_usage(2, res_instance) = ...
                res_instances_usage(2, res_instance) + task_time;            
            
            % !!!!!!!!!!task_time can be zero
            if (task_time),  % task_time is not zero
                %st = st + task_time;
                if eq(max_instances, 1),
                    disp([transition_name, ' [', num2str(start_time), ...
                        ' : ', num2str(end_time), ']']);                                
                else
                    disp(['(', resource_name, '-', ...
                        int2str(res_instance), '):   ',...
                        transition_name, ' [', num2str(start_time), ...
                        ' : ', num2str(end_time), ']']);            
                end;
            end;
        end;
    end;
    
    % summarize
    if eq(max_instances, 1),
        disp(['** ', resource_name, ':: Used ',...
                int2str(res_instances_usage(1,1)), ' times.  ']);
    else
        for j = 1:max_instances, 
            disp(['** (', resource_name, '-', int2str(j),  '):: Used ',...
                int2str(res_instances_usage(1,j)), ' times.  ', ...
                'Utilization time: ', int2str(res_instances_usage(2,j))]);
        end;
    end;
    
    %res_instances_usage
    st = sum(res_instances_usage(2, :));
    disp([resource_name, ':  Total Time spent: ', num2str(st)]); 
    resource_cost = res(current_res).resource_cost;
    resource_usage_cost = st * resource_cost; 
    if not(eq(resource_usage_cost, 0)), 
        disp([resource_name, ':  Total resource usage cost: ', ...
                num2str(resource_usage_cost)]); 
        Sum_Resource_Cost = Sum_Resource_Cost + resource_usage_cost;        
    end;
    ST(current_res) = st;
end;

% WRONG  !!!!!!!!!!!      % WRONG  K = SIGMA (instances) !!!!!!!!!!!
K = sum([PN.system_resources.max_instances]); % Number of server instances
LT = K * completion_time;  % line time
Total_time_at_Ks =  sum(ST);
LE = Total_time_at_Ks * 100/LT;  % line efficiency

SI = 0;
for i = 1:Rs, 
    si = completion_time - ST(i);
    SI = SI + si * si;
end;
%SI = sqrt(SI);
    
for ti = 1:Ts,
    Sum_Firing_cost = Sum_Firing_cost + ...
        PN.Set_of_Firing_Costs(ti)*PN.global_transitions(ti).times_fired;
    Sum_Fixed_cost = Sum_Fixed_cost + PN.Set_of_Fixed_Costs(ti);
end;
Total_Costs = Sum_Resource_Cost + Sum_Firing_cost + Sum_Fixed_cost;


disp('  ');

disp('  Summary: ');
disp('  ');
disp(['  Number of servers:  k = ', num2str(Rs)]);
disp(['  Total number of server instances:  K = ', num2str(K)]);
disp(['  Completion = ', num2str(completion_time)]);
disp(['  LT = ', num2str(LT)]);
disp(['  Total time at Stations: ', num2str(Total_time_at_Ks)]);
disp(['  LE = ', num2str(LE), ' %']);
%disp(['  SI = ', num2str(SI)]);
disp('  ** ');
disp(['  Sum resource usage costs: ', num2str(Sum_Resource_Cost), ...
    '   (', num2str(Sum_Resource_Cost*100/Total_Costs), '% of total)']);
disp(['  Sum firing costs: ', num2str(Sum_Firing_cost), ...
    '   (', num2str(Sum_Firing_cost*100/Total_Costs), '% of total)']);
disp(['  Sum fixed costs:  ', num2str(Sum_Fixed_cost), ...
    '   (', num2str(Sum_Fixed_cost*100/Total_Costs), '% of total)']);
disp(['  Total costs:  ', num2str(Total_Costs)]);
disp('  ** ');

