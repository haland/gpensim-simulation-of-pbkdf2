function [new_event] = create_new_event_in_Q(transition1, ...
    delta_X, output_place)
% [new_event_in_Q] = create_new_event_in_Q(trans,deltaX,outputPl)
%
%               Reggie Davidrajuh (c) August 2011
%

global PN;

new_event.start_time = PN.current_time; %it can be local time

% completion time
ftime = PN.Set_of_Firing_Times(transition1);

if isnan(ftime),  % firing time is a string; e.g. 'unifrnd(10, 12)'
    trans = PN.global_transitions(transition1);
    ftime = eval(trans.firing_time);
end;
new_event.completion_time = PN.current_time + ftime; % completion time

new_event.delta_X = delta_X; %tokens to be deposited
new_event.output_place = output_place; %output places
new_event.event = transition1;
new_event.from_State = PN.State;


% event_in_Q = 
% 
%       start_time: 518
%       completion: 519.7500
%          delta_X: [0 0 1]
%     output_place: [0 0 1]
%            event: 2
%        add_color: {}