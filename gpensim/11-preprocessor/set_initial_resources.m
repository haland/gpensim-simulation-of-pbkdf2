function [] = set_initial_resources(dynamicpart)
%        [] = set_initial_resources(dynamicpart)

%  Reggie.Davidrajuh@uis.no (c) Version 6.0 (c) 10 july 2012  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global PN;

Ts = PN.No_of_transitions;
Resources = [];

% process the resource names
if isfield(dynamicpart, 'resources'), 
    resources = dynamicpart.resources;
    Rs = length(resources)/2;  % number of (resources with instances)
    for i = 1:Rs,
        res.name = resources{2*i - 1};
        res.max_instances = resources{2*i};    
        if or(not(ischar(res.name)), not(isnumeric(res.max_instances))), 
            error('"dynamic.resources" must contain (name value) pairs'); 
        end;
        res.on_use = 0; % no one is using this resource now
        res.resource_cost = 0; % cost of resource is yet to assigned
        Resources = [Resources res];
    end;
end;
PN.system_resources = Resources;

% create the matrix to record start of use of each instance of resources 
for i = 1:Rs,
    max_inst = PN.system_resources(i).max_instances;

    % first row of "start_of_use" will be the transition, 
    % second row is the start time 
    PN.system_resources(i).start_of_use = zeros(2, max_inst);
end;

% for each transition, add the register for resource reservation
for i = 1:Ts,
    PN.global_transitions(i).resources_reserved = zeros(1, Rs);
end;

% process the resource costs
if isfield(dynamicpart, 'resource_costs'), 
    resource_costs = dynamicpart.resource_costs;
    costs = length(resource_costs)/2;  % number of (resources with cost)
    for i = 1:costs,
        res_name = resource_costs{2*i - 1};
        res_cost = resource_costs{2*i};    
        ri = check_valid_resource(res_name);
        PN.system_resources(ri).resource_cost = res_cost;
    end;
 end;

PN.No_of_system_resources = Rs;
PN.Resource_usage_LOG = [];
