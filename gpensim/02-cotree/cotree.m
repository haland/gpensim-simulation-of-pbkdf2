function [COTREE] = cotree(pni, plot_ct, print_ct)   
%        [COTREE] = cotree(pni, plot_ct, print_ct)   
% Name:	cotree
% Purpose:	Creates the coverability tree of a Petri net 
%           and then print and plot it
% Input parameters:	Static Petri net sturcture (output by ‘petrinetgraph’)
%                   Intial_markings
% Uses:	print_cotree, plot_cotree
%       
% Used by:	[main simulation file]
% NOTE:	  plot_cotree is based on the work by Univ. Cagliari
% Out parameters:	[]
%
% Example:	
%   % in main simulation file
%   png = petrinetgraph('cotree_example_def');
%   dyn.initial_markings = {'p1',2, 'p4', 1};
%   cotree(png, dyn.initial_markings);
%

%   Reggie.Davidrajuh@uis.no (c) Version 6.0 (c) July 2012 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global PN;
PN = pni;
X0 = pni.X;

[COTREE] = build_cotree(X0); 

if ge(nargin, 2), % now plot the cotree with the function plot_cotree
    if plot_ct, plot_cotree(X0); end;
end;

if eq(nargin, 3), % now print ASCII cotree 
    if print_ct, print_cotree(COTREE); end;
end;
