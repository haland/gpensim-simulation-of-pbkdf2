function [acquired] = resource_request_one_res(t_index, ...
                     r_index, no_of_instances_required)
% function [acquired] = resource_request_one_res(t_index, ...
%                     r_index, no_of_instances_required)

%  Reggie.Davidrajuh@uis.no (c) Version 6.0 (c) 10 july 2012  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global PN;

acquired = false; % initially 

max_inst = PN.system_resources(r_index).max_instances;
on_use =   PN.system_resources(r_index).on_use;
already_reserved = PN.global_transitions(t_index).resources_reserved(r_index);

available_instances = max_inst - on_use - already_reserved; 

if le(no_of_instances_required, available_instances),
    PN.global_transitions(t_index).resources_reserved(r_index) = ...
        PN.global_transitions(t_index).resources_reserved(r_index) + ...
            no_of_instances_required;
        
    acquired = true;
end;  

