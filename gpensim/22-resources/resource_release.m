function [] = resource_release(trans_name)
% function [] = resource_release(trans_name)
% this function releases all the resources the specified transition
% is holding.
%
% Usage:
%   Inputs:
%       trans_name: name of the specified transition
%
%   Outputs: None
% 

%  Reggie.Davidrajuh@uis.no (c) Version 6.0 (c) 10 july 2012  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global PN;

t_index = check_valid_transition(trans_name);

if not(isfield(PN.global_transitions(t_index), 'resources_owned')), return; end;

resources_owned = PN.global_transitions(t_index).resources_owned;
if not(sum(resources_owned)), return; end; % there are no resource owned

Rs = PN.No_of_system_resources;
res_released_time = PN.current_time; % resource released time
    
for i = 1:Rs, % check resource by resource
    rou = resources_owned(i);    
    if rou,
        % first: decrement on_use
        PN.system_resources(i).on_use= PN.system_resources(i).on_use - rou;
        
        % clear the 'start_of_use' matrix and copy start_time to LOG
        start_of_use = PN.system_resources(i).start_of_use;
        on_use_by_t = find(start_of_use(1,:)== t_index);
        for j = 1:length(on_use_by_t), % instance by instance
            current_inst = on_use_by_t(j);
            start_time = start_of_use(2, current_inst);
            PN.system_resources(i).start_of_use(1, current_inst) = 0;
            use_log=[i,t_index,start_time,res_released_time,current_inst];
            PN.Resource_usage_LOG = [PN.Resource_usage_LOG; use_log];
        end;    
    end;  % if rou
end;

% fourth: clear ownership of resources
PN.global_transitions(t_index).resources_owned = zeros(1, Rs);
