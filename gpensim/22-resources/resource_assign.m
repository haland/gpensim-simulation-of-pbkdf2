function [resource_usage_cost] = resource_assign(t_index)
% function [resource_usage_cost] = resource_assign(t_index)
% this function cancells all the resources that are reserved by 
% a transition in its _pre file.  
%
% In a _pre file, a transition can ONLY reserve resources:
%    if the resources are available, then they will be reserved
%    for the transition. However, only when the transition 
%    starts firing, then the reserved resources will be allocated 
%    to the firing transition. On the other hand, if a transiton fails
%    to satisfy the firing_conditions and do not start fire, 
%    then all the resource reservations will be cancelled by this 
%    function.
%
% Usage:
%   Inputs:
%       trans_name: name of the specified transition
%
%   Outputs:
%       allocated: name of the specified transition
%
%   Called by: firing_preconditions
%

%  Reggie.Davidrajuh@uis.no (c) Version 6.0 (c) 10 july 2012  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

global PN;

resource_usage_cost = 0;   % initially 
rr = PN.global_transitions(t_index).resources_reserved;

if not(sum(rr)), 
    return;  % resource_usage_cost = 0;   
end; % if no resource reservation, simply return

Rs = PN.No_of_system_resources;
firing_time =  PN.Set_of_Firing_Times(t_index);

% first: assign resources to tx 
PN.global_transitions(t_index).resources_owned = rr; 

% second: clear the reservation list 
PN.global_transitions(t_index).resources_reserved = zeros(1, Rs);

% third: increase "on_use", and 
% fourth: update "start_of_use"
for i = 1:Rs,
    no_of_instances_in_use = rr(i);
    if no_of_instances_in_use,    
        % setting the start time: first find the avialble instance
        start_of_use = PN.system_resources(i).start_of_use;
        % row 1 of "start_of_use" is the t_indices
        all_instances = start_of_use(1, :);
        free_instances = find(not(all_instances));
        for j = 1:no_of_instances_in_use,
            start_of_use(1, free_instances(j)) = t_index;
            start_of_use(2, free_instances(j)) = PN.current_time;
        end;
        resource_inst_cost = PN.system_resources(i).resource_cost;
        resource_cost = resource_inst_cost * no_of_instances_in_use; %%%%%% rr(i) is corrected
        resource_usage_cost = resource_usage_cost + ...
                                (firing_time * resource_cost);
        PN.system_resources(i).start_of_use = start_of_use;
        PN.system_resources(i).on_use = PN.system_resources(i).on_use + rr(i);
    end;
end;
