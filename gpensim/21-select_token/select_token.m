function [set_of_tokID] = select_token(placeI, nr_tokens_wanted)
% [set_of_tokID] = select_token(placeI, nr_tokens_wanted)
% function [set_of_tokID] = select_token(placeI, nr_tokens_wanted)

global PN;


set_of_tokID = [];
if not(nr_tokens_wanted),  % nr_tokens_wanted == 0
    return;
end;

% place is a character string
p_index = search_names(placeI, PN.global_places);
if (p_index),
    placeI = p_index;
else
    error([placeI, ': wrong place name in "select_token"']);
end;    

nr_tokens_in_placeI = PN.global_places(placeI).tokens;
if not(nr_tokens_in_placeI),  % this place has no tokens 
    return;
end;

if gt(nr_tokens_wanted, nr_tokens_in_placeI),
    nr_tokens_wanted = nr_tokens_in_placeI;  % number of tokens available
end;

token_bank = PN.global_places(placeI).token_bank(1:nr_tokens_wanted);
set_of_tokID = token_bank.tokID;
