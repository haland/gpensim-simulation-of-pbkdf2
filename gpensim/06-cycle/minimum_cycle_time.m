function [V] = minimum_cycle_time(png, dyn)
% function [V] = minimum_cycle_time(png, dyn)
% This function finds the "minim-cycle-time (MCT)" of a marked graph.
% In order to find MCT, this function first lists all the cycles 
% in a marked_graph. The MCT = max(cycle Delay/tokens in the cycle)
% 
% This function first checks whether the PN is a mrked_graph.
% If not, it simply exits with an error message.
% If this is a marked graph, 
%  1) PN (incidence matrix) is converted into 
%       standard V (adjancency matirx) for graph algorithms 
%       using the function "convert_PN_V"
%  2) all the cycles are found using the function "cycles.m"
%  
%       Reggie.Davidrajuh@uis.no (c) December 2011


[classtype] = petrinetclass(png);
if not(classtype(3)), % png is is NOT a marked graph
    error('This is not a marked graph ....');
    V = [];
    return;
end;
    
V1 = convert_PN_V(png);
V = cycles(V1);

No_of_nodes = length(V.nodes);

% init
for i = 1:No_of_nodes,
    V.nodes(i).imarkings = 0; V.nodes(i).firing_time = 0;
end;
    
% extracting markings; check whether current place is member of V
% and set markings in V.node(placeI) 
imarkings = dyn.m0;
no_of_places = length(imarkings)/2; %number of elements to be extracted
for i=1:no_of_places,
    curr_place_name = imarkings{2*i -1};
    place_nr = search_names(curr_place_name, V.nodes); 
    if (place_nr == 0), %wrong transition name
        error([curr_place_name, ': No such place name']);
    end;
        
    % assign firing time to transitions
    markings = imarkings{2*i};
    V.nodes(place_nr).imarkings = markings;  
end;

% extracting firingtimes; check whether current trans is member of V
% and set ftimes in V.node(transI) 
ftimes = dyn.ft;
no_of_trans = length(ftimes)/2; %number of elements to be extracted
for i=1:no_of_trans,
    curr_trans_name = ftimes{2*i -1};
    trans_nr = search_names(curr_trans_name, V.nodes); 
    if (trans_nr == 0), %wrong transition name
        error([curr_trans_name, ': No such transition name']);
    end;
        
    % assign firing time to transitions
    ft = ftimes{2*i};
    V.nodes(trans_nr).firing_time = ft;  
end;

% now print: cycle number, cycle path, total Time delay, token sum, CT
print_minimum_cycle_time(V);
