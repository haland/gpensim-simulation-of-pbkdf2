function [fire, transition] = tHashedBlock_pre(transition)

global global_info;

% Select token from pBlock with color equal to the desired number of hash
% iterations
tokID1 = select_token_with_colors('pBlock',1,...
    int2str(global_info.iterations));
transition.selected_tokens = tokID1;

% If the token exists, override and set the tokens color to the current
% block number and reset the iteration counter
if tokID1 > 0
    transition.new_color = int2str(global_info.block_counter);
    transition.override = 1;

    global_info.iteration_counter = 0; 
end

fire = (tokID1);