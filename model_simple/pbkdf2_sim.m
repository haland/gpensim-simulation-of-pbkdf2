% Clear workspace for previously stored variables
clear all; clc;

% Initialize global info variable
global global_info;

% Weight added to the timing of transactions
global_info.WEIGHT = 0.01;

% Let the model loop an 'infinite' number of times
global_info.MAX_LOOP = Inf;

% The password bit-length (ADJUSTABLE)
global_info.password_length = 128;

% The desired bit-size of the PBKDF2 output (ADJUSTABLE)
global_info.master_key_length = 1024;

% The bit-size of the hashing algorithm (SHA-256) output (FINAL)
global_info.hash_digest_size = 256;

% Number of blocks to compute hashes for (CALCULATED)
global_info.blocks = global_info.master_key_length/...
    global_info.hash_digest_size;

% Number of block hashing iterations (ADJUSTABLE)
global_info.iterations = 50;

% Conuter for number of blocks that has been processed (for coloring)
global_info.block_counter = 0;

% Counter for number of iterations on a single block (for coloring)
global_info.iteration_counter = 0;

% Load Petri-net graph
png = petrinetgraph('pbkdf2_sim_def');

% Initial state of petri net
dyn.m0 = {'pPassword', 1};

% Time units for transitions
dyn.ft = {'tCreateBlocks',1 * global_info.WEIGHT,...
    'tIterator',1 * global_info.WEIGHT,...
    'tHash',ceil(global_info.password_length/512) * 79 * ...
        global_info.WEIGHT,...
    'tHashedBlock',1 * global_info.WEIGHT,...
    'tHashedPassword',1 * global_info.WEIGHT};

pni = initialdynamics(png, dyn);
sim = gpensim(pni);

% Print token and model results
%%cotree(pni, 1, 1);
%%plotp(sim, {'pPassword','pBlocks','pBlock','pResult'});

% Plot color results for the model
%%print_finalcolors(sim);
%%print_colormap(sim, {'pPassword','pBlocks','pBlock','pResult'});
