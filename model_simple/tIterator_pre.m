function [fire, transition] = tIterator_pre(transition)

global global_info;

% Increase the block counter by one
global_info.block_counter = global_info.block_counter + 1;

% Assign higher priority for tHashedPassword than tIterator. When the
% counter reaches the block limit, tHashedPassword will get the token.
priority_assign('tIterator', 0);
priority_assign('tHashedPassword', 1);

% Override the token color and set it to the current block number
transition.new_color = 'new_block';
transition.override = 1;

fire = 1;