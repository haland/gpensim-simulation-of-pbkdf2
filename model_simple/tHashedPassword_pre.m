function [fire, transition] = tHashedPassword_pre(transition)

global global_info;

% Select token from pBlocks with color equal to the number of block
% iterations.
tokID1 = select_token_with_colors('pBlocks',1,...
    int2str(global_info.blocks));
transition.selected_tokens = tokID1;

% If the token is found, reset the block counter
if tokID1 > 0
    global_info.block_counter = 0;
end

fire = (tokID1);