function [png] = pbkdf2_sim_def()

png.PN_name = 'PBKDF2 algorithm simulation';

png.set_of_Ps = {'pPassword','pBlocks','pBlock','pResult'};

png.set_of_Ts = {'tCreateBlocks','tIterator','tHash','tHashedBlock',...
    'tHashedPassword',...
};

png.set_of_As = {'pPassword','tCreateBlocks',1,'tCreateBlocks',...
    'pBlocks',1,'pBlocks','tIterator',1,'tIterator','pBlock',1,...
    'pBlock','tHash',1,'tHash','pBlock',1,'pBlock','tHashedBlock',1,...
    'tHashedBlock','pBlocks',1,'pBlocks','tHashedPassword',1,...
    'tHashedPassword','pResult',1,...
}; 
