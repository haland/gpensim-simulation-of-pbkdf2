function [fire, transition] = tHash_pre(transition)

global global_info;
% Increase the iteration counter by one
global_info.iteration_counter = global_info.iteration_counter + 1;

% Assign higher priority for tHashedBlock than tHash. When the
% counter reaches the iteration limit, tHashedBlock will get the token.
priority_assign('tHash', 0);
priority_assign('tHashedBlock', 1);

% Override and set the token color to the iteration counters current value
transition.new_color = int2str(global_info.iteration_counter);
transition.override = 1;

fire = 1;