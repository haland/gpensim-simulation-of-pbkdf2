function [fire, transition] = tHashedPassword_pre(transition)

global global_info;

% Select token from pBlocks with color equal to the maximum PBKDF2 blocks
% count
tokID1 = select_token_with_colors('pBlocks',1,...
    int2str(global_info.pbkdf2_blocks_max));
transition.selected_tokens = tokID1;

% If the token exists, reset the PBKDF2 block counter
if tokID1 > 0
    global_info.pbkdf2_blocks_counter = 0;
end

fire = (tokID1);