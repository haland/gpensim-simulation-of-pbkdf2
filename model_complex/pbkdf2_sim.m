% Clear workspace for previously stored variables
clear all; clc;

% Initialize global info variable
global global_info;

% Let the model loop an 'infinite' number of times (FINAL)
global_info.MAX_LOOP = Inf;

% The weight added to the time definitions for transactions (ADJUSTABLE)
global_info.WEIGHT = 0.1;

% The password length (in bits) for the system (ADJUSTABLE)
global_info.password_length = 1024;

% The desired bit-size of the PBKDF2 output (ADJUSTABLE)
global_info.master_key_length = 512;

% The bit-size of the hashing algorithm (SHA-256) output (FINAL)
global_info.hash_digest_size = 256;

% Variables for iteration through blocks in PBKDF2
global_info.pbkdf2_blocks_max = global_info.master_key_length/...
        global_info.hash_digest_size; % FINAL
global_info.pbkdf2_blocks_counter = 0; % FINAL

% Variables for hash iterations in PBKDF2
global_info.pbkdf2_iteration_max = 2; % ADJUSTABLE
global_info.pbkdf2_iteration_counter = 0; % FINAL

% Variables for 512-bit block iteration in SHA-256
global_info.sha512_blocks_max =...
    ceil(global_info.password_length/512); % FINAL
global_info.sha512_blocks_counter = 0; % FINAL

% Variables for hashing in SHA-256
global_info.hash_blocks_max = 64; % FINAL
global_info.hash_blocks_counter = 0; % FINAL

% Load Petri-net graph
png = petrinetgraph('pbkdf2_sim_def');

% Initial state of petri net
dyn.m0 = {'pPassword', 1};

% Time units for transitions
dyn.ft = {'tCreateBlocks',1 * global_info.WEIGHT,...
    'tIterator',1 * global_info.WEIGHT,...
    'tBlockify512',1 * global_info.WEIGHT,...
    't512Iterator',1 * global_info.WEIGHT,...
    'tBlockify32',1 * global_info.WEIGHT,...
    'tHash',1 * global_info.WEIGHT,...
    't512Reassemble',1 * global_info.WEIGHT,...
    't512Finished',1 * global_info.WEIGHT,...
    'tHashedBlock',1 * global_info.WEIGHT,...
    'tHashedPassword',1 * global_info.WEIGHT,...
};

pni = initialdynamics(png, dyn);
sim = gpensim(pni);

% Print token and model results
%%cotree(pni, 1, 1);
%%plotp(sim, {'pResult'});

% Plot color results for the model
%%print_finalcolors(sim);
%%print_colormap(sim, {'pPassword','pBlocks','pBlock','p512Blocks',...
%%    'p512Block','p32Blocks','pResult'});
