function [fire, transition] = t512Iterator_pre(transition)

global global_info;

% Increase the SHA block iteration counter by one (new round)
global_info.sha512_blocks_counter = global_info.sha512_blocks_counter + 1;

% Assign higher priority to the t512Finished over t512Iterator, so that
% t512Finished fires if its conditions are met.
priority_assign('t512Iterator', 0);
priority_assign('t512Finished', 1);

transition.new_color = 'sha512_blocks';
transition.override = 1;

fire = 1;