function [fire, transition] = t512Reassemble_pre(transition)

global global_info;

% Select token from p32Blocks with color equal to the maximum block hashing
% count
tokID1 = select_token_with_colors('p32Blocks',1,...
    int2str(global_info.hash_blocks_max));
transition.selected_tokens = tokID1;

% If the token exists, reset the block hashing counter and set the tokens
% color to the current value of the SHA block iteration counter.
if tokID1 > 0
    transition.new_color = int2str(global_info.sha512_blocks_counter);
    transition.override = 1;

    global_info.hash_blocks_counter = 0;
end

fire = (tokID1);