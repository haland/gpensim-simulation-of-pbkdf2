function [fire, transition] = tIterator_pre(transition)

global global_info;

% Increase the PBKDF2 block counter value by one
global_info.pbkdf2_blocks_counter = global_info.pbkdf2_blocks_counter + 1;

% Assign higher priority to tHashedPassword over tIterator, so that
% tHashedPassword fires if its conditions are met.
priority_assign('tIterator', 0);
priority_assign('tHashedPassword', 1);

transition.new_color = 'pbkdf2_blocks';
transition.override = 1;

fire = 1;