function [fire, transition] = tBlockify512_pre(transition)

global global_info;

% Increase the PBKDF2 iteration counter by one
global_info.pbkdf2_iteration_counter = ...
    global_info.pbkdf2_iteration_counter + 1;

% Assign higher priority to tHashedBlock over tBlockify512, so that
% tHashedBlock fires if its conditions are met.
priority_assign('tBlockify512', 0);
priority_assign('tHashedBlock', 1);

transition.new_color = 'sha_blocks';
transition.override = 1;

fire = 1;