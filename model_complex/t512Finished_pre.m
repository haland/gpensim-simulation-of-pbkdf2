function [fire, transition] = t512Finished_pre(transition)
global global_info;

% Select token from p512Blocks with color equal to the maximum number of
% SHA block iterations (finished the hashing iterations)
tokID1 = select_token_with_colors('p512Blocks',1,...
    int2str(global_info.sha512_blocks_max));
transition.selected_tokens = tokID1;

% If the token is selected, reset the SHA block iteration counter and
% set the tokens color to the current hashing iteration of the 
% PBKDF2-algoritm.
if tokID1 > 0
    transition.new_color = int2str(global_info.pbkdf2_iteration_counter);
    transition.override = 1;
    
    global_info.sha512_blocks_counter = 0;
end

fire = tokID1;