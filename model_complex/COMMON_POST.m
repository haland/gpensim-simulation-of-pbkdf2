function [fire, transition] = COMMON_POST (transition)

global global_info;

% If post transition is for tHashedPassword, the transition stops the
% simulation. This is the final step of the algorithm
if strcmp(transition.name, 'tHashedPassword')
   global_info.STOP_SIMULATION = 1; 
end