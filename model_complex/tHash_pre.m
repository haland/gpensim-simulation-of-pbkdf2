function [fire, transition] = tHash_pre(transition)

global global_info;

% Increase the hashing block counter value by one
global_info.hash_blocks_counter = global_info.hash_blocks_counter + 1;

% Assign higher priority to t512Reassemble over tHash, so that
% tHashedBlock fires if its conditions are met.
priority_assign('tHash', 0);
priority_assign('t512Reassemble', 1);

% Set the tokens color equal to the current value of the hashing block
% counter
transition.new_color = int2str(global_info.hash_blocks_counter);
transition.override = 1;

fire = 1;