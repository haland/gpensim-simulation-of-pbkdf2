function [fire, transition] = tHashedBlock_pre(transition)

global global_info;

% Select token from pBlock with color equal to the maximum PBKDF2 iteration
% count
tokID1 = select_token_with_colors('pBlock',1,...
    int2str(global_info.pbkdf2_iteration_max));
transition.selected_tokens = tokID1;

% If the token exists, reset the PBKDF2 iteration counter and set the color
% of the token equal to the PBKDF2 block counters value.
if tokID1 > 0
    transition.new_color = int2str(global_info.pbkdf2_blocks_counter);
    transition.override = 1;

    global_info.pbkdf2_iteration_counter = 0;
end

fire = (tokID1);